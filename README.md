# Seed17_CanSat
Seed17's CanSat pcb

A CanSat pcb design to support Various sensors including:
Lora, thermistor, photoresistor, gps neomv2, microsd, bluetooth_hc_05, mpu280.

Designed in eagle, Version 9.3.0
pcb diameter: 5cm

The small circular pcb is the external programmer.

Manual Χρήσης:
Setup: 
-Συνδέεται το step up στη μπαταρία και με έξοδο 5v συνδέεται η έξοδος του step up στο pcb. 
-Συνδέεται το gps στο pcb

Testing: 
-Βραχικυκλώνονται τα 2 short pins του step up
-Αυξάνεται σιγά σιγά τη τάση του step up μέχρι να παρατηρηθεί πάνω στο pcb,  μετά τις διόδους  τάση 5V.
-(μετά τον προγραμματισμό να ελεγχθεί αν με το mpu 280 τα atmega επικοινωνούν επιτυχώς).
-Δεν ειναι πλεον βραχυκυκλωμένα τα 2 short pins του step up.

Jumpers:
-Συνδέονται jumper wires από τα 2 short pins του step up του CANSAT και εκτίθενται έξω.
-Συνδέονται jumper wires (7 pins) από τον αντάπτορα εσωτερικά του pcb, τα οποία θα είναι προσβάσιμα στον προγραμματιστή, στο εξωτερικο μερος του cansat ( εκτίθενται έξω).

Για τον προγραμματισμό: 
-Δεν βραχυκυκλωνονται τα 2 short pins του step up, για να μη τροφοδοτηθουν τα atmega.
-Συνδέεται το 5v FTDI στον εξωτερικό προγραμματιστή.
-Συνδέεται ο εξωτερικός προγραμματιστής στον υπολογιστή και στα jumper wires που προεξέχουν από το cansat
-επιλέγεται από το dip switch ποιο atmega θα προγραμματιστεί (τροφοδοτείται το προγραμματιζόμενο atmega και απενεργοποιείται η τροφοδοσία του άλλου.
-Ανεβάζεται το πρόγραμμα στο επιλεγμένο atmega
-Απενεργοποιούμε τη τροφοδοσία στο atmega ου προγραμματίστηκε και ενεργοποιείται η τροφοδοσία του άλλου΄.
-Ανεβάζεται το πρόγραμμα στο επιλεγμένο atmega
-Αφαιρούμε τον προγραμματιστή
-Βραχυκυκλωνονται τα 2 pins του step up για να ξεκινήσει η λειτουργία των atmega.


<div dir="ltr" style="text-align: left;" trbidi="on">
<div class="separator" style="clear: both; text-align: center;">
<a href="https://2.bp.blogspot.com/-dY8h_RqjXW4/XGV6g_pyehI/AAAAAAAAAAU/iXmMhloUagIJESz7AvawlCLZxzpkJ99aQCLcBGAs/s1600/Untitled.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" data-original-height="900" data-original-width="1600" height="360" src="https://2.bp.blogspot.com/-dY8h_RqjXW4/XGV6g_pyehI/AAAAAAAAAAU/iXmMhloUagIJESz7AvawlCLZxzpkJ99aQCLcBGAs/s640/Untitled.png" width="640" /></a></div>
<div class="separator" style="clear: both; text-align: center;">
<br /></div>
<div class="separator" style="clear: both; text-align: center;">
<a href="https://4.bp.blogspot.com/-lES46GLkjio/XGV6zEdS1WI/AAAAAAAAAAg/5KX5dxuGDykEzpSfj5J7vjQSoXDzqoKXQCLcBGAs/s1600/Untitled.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" data-original-height="900" data-original-width="1600" height="360" src="https://4.bp.blogspot.com/-lES46GLkjio/XGV6zEdS1WI/AAAAAAAAAAg/5KX5dxuGDykEzpSfj5J7vjQSoXDzqoKXQCLcBGAs/s640/Untitled.png" width="640" /></a></div>
<div class="separator" style="clear: both; text-align: center;">
<br /></div>
<br /></div>

[![IMG-20190312-144750.jpg](https://i.postimg.cc/ydJy31JB/IMG-20190312-144750.jpg)](https://postimg.cc/zbrg91SP)

[![IMG-20190307-160246.jpg](https://i.postimg.cc/jdyvhSMS/IMG-20190307-160246.jpg)](https://postimg.cc/jLdzxtVp)

[![IMG-20190307-160255.jpg](https://i.postimg.cc/DwF6p3Nd/IMG-20190307-160255.jpg)](https://postimg.cc/VdGX5pmd)

[![IMG-20190307-160310.jpg](https://i.postimg.cc/L8TTr0Kd/IMG-20190307-160310.jpg)](https://postimg.cc/TKKgb03Q)

[![IMG-20190414-225843.jpg](https://i.postimg.cc/JhYQhDzP/IMG-20190414-225843.jpg)](https://postimg.cc/PLZD3qZw)

[![IMG-20190514-001941.jpg](https://i.postimg.cc/KY8NFrx1/IMG-20190514-001941.jpg)](https://postimg.cc/0zhm0S6v)

[![IMG-20190514-002000.jpg](https://i.postimg.cc/XNRQFCcP/IMG-20190514-002000.jpg)](https://postimg.cc/rz912zHG)
