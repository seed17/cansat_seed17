#include <Wire.h>
#include <SPI.h>
#include <LoRa.h>
bool read_Slave = false, read_Json_Slave_OK = false;
bool flag = true;
String text = "";

void setup()
{
  Serial.begin(115200);           // start serial for output
  Serial.println(F("\n\n    Seed17 Project"));
  Serial.println(F("    Questions: info@seed17.eu\n"));
  Serial.println("\n      MASTER ARDUINO!");
  delay(2000);

  Wire.begin(8);                // join i2c bus with address #8
  Wire.onReceive(receiveEvent); // register event

  if (!LoRa.begin(915E6)) {
    Serial.println("Starting LoRa failed!");
    while (1);
  }

  Serial.println(F("\nSeed17 Project"));
  Serial.println(F("Questions: info@seed17.eu\n"));
}

void loop()
{
  if (flag) Serial.println(F("\nloop()\n"));
  flag = false;

  receiveEvent();
  //  Serial.println("\nSend to Slave A...");
  //  Wire.beginTransmission(11); // transmit to device #8
  //  Wire.write("A");
  //  Wire.endTransmission();    // stop transmitting

  //  currentMillis = millis();
  //  startMillis = currentMillis;
  //  while (currentMillis - startMillis <= period_Slave && read_Json_Slave_OK == false)
  //  {
  //    currentMillis = millis();
  //    delay(1);
  //    //Serial.println("...");
  //  }
  //  Serial.print("Time to read data Slave A is : " + String(currentMillis - startMillis));
  //
  //  Serial.println("\nTime to read all data is : " + String(millis() - start_connection));
  //  delay(500);
}

// function that executes whenever data is received from master
// this function is registered as an event, see setup()
void receiveEvent() {
  char c;
  bool whileFlag = true;
  while (Wire.available() && whileFlag) {
    c = Wire.read();
    //    Serial.println(c);
    text = text + String(c);
    Serial.println(text);
    if (c == 'b') {
      whileFlag = false;
      Serial.println(text); // send packet
      LoRa.beginPacket();
      LoRa.print(text);
      LoRa.endPacket();
      text = "";
    }
    //    Serial.println(c);
    //Serial.println(current_position_array_Slave_A);
  }
}
