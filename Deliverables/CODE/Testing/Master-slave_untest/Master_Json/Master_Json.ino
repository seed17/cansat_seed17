#include <ArduinoJson.h>
#include <Wire.h>

StaticJsonDocument<200> doc;
JsonObject rootTemp = doc.to<JsonObject>();
JsonObject rootSlave = doc.to<JsonObject>();

char input_Json_Slave[150];

int current_position_array_Slave = 0;
bool read_Slave = false,read_Json_Slave_OK = false;

unsigned long startMillis,start_connection;
unsigned long currentMillis;
int period_Slave = 100;
String dataMPU6050 = "";

void setup() 
{  
  Wire.begin(8);                // join i2c bus with address #8
  Wire.onReceive(receiveEvent); // register event
  
  Serial.begin(115200);           // start serial for output
  Serial.println(F("\n\n    Seed17 Project"));
  Serial.println(F("    Questions: info@seed17.eu\n"));
  Serial.println("\n      MASTER ARDUINO!");
  delay(2000);
}

void loop() 
{
  start_connection = millis();
 
  current_position_array_Slave = 0;
  read_Json_Slave_OK = false;
  
  Serial.println("\nSend to Slave A...");
  Wire.beginTransmission(11); // transmit to device #8
  Wire.write("A");
  Wire.endTransmission();    // stop transmitting
  
  currentMillis = millis();
  startMillis = currentMillis;
  while(currentMillis - startMillis <= period_Slave && read_Json_Slave_OK == false)
  {
    currentMillis = millis();
    delay(1);
    //Serial.println("...");
  }
  Serial.print("Time to read data Slave A is : " + String(currentMillis - startMillis));
  
  Serial.println("\nTime to read all data is : " + String(millis() - start_connection));
  delay(500);
}

// function that executes whenever data is received from master
// this function is registered as an event, see setup()
void receiveEvent(int howMany) 
{
  char c;

  while (Wire.available()) 
  {
    c = Wire.read();
    input_Json_Slave[current_position_array_Slave] = c;
    current_position_array_Slave++;
    if(c == '}')
    {
      read_Json_Slave_OK = true;
    }
    //Serial.print(c);
    //Serial.println(current_position_array_Slave_A);
  }
  
}
