#include <Wire.h>
#include <BMx280MI.h> //gy-91
#include <MPU9255.h>  //gy-91
#include <Adafruit_Sensor.h>
#include <DHT.h>
#include <DHT_U.h>
#include <SPI.h>
#include <SD.h>
#include <SoftwareSerial.h>
#include <TinyGPS.h>

#define I2C_ADDRESS 0x76
#define DHTPIN A0
#define DHTTYPE    DHT11

//create a objects
BMx280I2C bmx280(I2C_ADDRESS);
MPU9255 mpu;
DHT_Unified dht(DHTPIN, DHTTYPE);
SoftwareSerial mySerial(8, 7); // RX, TX
TinyGPS gps;

int Send = false;
uint32_t delayMS;
const int chipSelect = 4;
String dataString = "", location = "";
long int gpsPosition = 0;

//void print_data() {
//  mpu.read_acc();
//  mpu.read_gyro();
//
//  dataString += mpu.ax;
//  dataString += mpu.ay;
//  dataString += mpu.az;
//  dataString += mpu.gx;
//  dataString += mpu.gy;
//  dataString += mpu.gz;
//
//  dataString += mpu.ax;
//  dataString += mpu.ay;
//  dataString += mpu.az;
//  dataString += mpu.gx;
//  dataString += mpu.gy;
//  dataString += mpu.gz;
//  delay(100);
//}

void adjust_offset() {
  //set bandwidths to 5Hz to reduce the noise
  mpu.set_acc_bandwidth(acc_5Hz);
  mpu.set_gyro_bandwidth(gyro_5Hz);

  //gyroscope & accelerometer X axis offset, Y axis offset, Z axis offset
  int16_t gX_offset = 0, gY_offset = 0, gZ_offset = 0, aX_offset = 0, aY_offset = 0, aZ_offset = 0;

  //update flags
  //gyroscope & accelerometer
  bool update_gX = true, update_gY = true, update_gZ = true;
  bool update_aX = true, update_aY = true, update_aZ = true;

  //discard the first reading
  mpu.read_acc();
  mpu.read_gyro();
  delay(10);

  while (1) //offset adjusting loop
  {
    mpu.read_acc();
    mpu.read_gyro();

    //-------- adjust accelerometer X axis offset ----------
    if (mpu.ax > 0 && update_aX == true) {
      aX_offset --;//decrement offset
    }

    if (mpu.ax < 0 && update_aX == true)  {
      aX_offset ++;//increment offset
    }

    //-------- adjust accelerometer Y axis offset ----------
    if (mpu.ay > 0 && update_aY == true) {
      aY_offset --;//decrement offset
    }

    if (mpu.ay < 0 && update_aY == true) {
      aY_offset ++;//increment offset
    }

    //-------- adjust accelerometer Z axis offset ----------
    if (mpu.az > 0 && update_aZ == true) {
      aZ_offset --;//decrement offset
    }

    if (mpu.az < 0 && update_aZ == true) {
      aZ_offset ++;//increment offset
    }

    //-------- adjust gyroscope X axis offset ----------
    if (mpu.gx > 0 && update_gX == true) {
      gX_offset --;//decrement offset
    }

    if (mpu.gx < 0 && update_gX == true) {
      gX_offset ++;//increment offset
    }

    //-------- adjust gyroscope Y axis offset ----------
    if (mpu.gy > 0 && update_gY == true) {
      gY_offset --;//decrement offset
    }

    if (mpu.gy < 0 && update_gY == true) {
      gY_offset ++;//increment offset
    }

    //-------- adjust gyroscope Z axis offset ----------
    if (mpu.gz > 0 && update_gZ == true) {
      gZ_offset --;//decrement offset
    }

    if (mpu.gz < 0 && update_gZ == true) {
      gZ_offset ++;//increment offset
    }

    //set new offset
    if (update_gX == true) {
      mpu.set_gyro_offset(X_axis, gX_offset);
    }

    if (update_gY == true) {
      mpu.set_gyro_offset(Y_axis, gY_offset);
    }

    if (update_gZ == true) {
      mpu.set_gyro_offset(Z_axis, gZ_offset);
    }

    if (update_aX == true) {
      mpu.set_acc_offset(X_axis, aX_offset);
    }

    if (update_aY == true) {
      mpu.set_acc_offset(Y_axis, aY_offset);
    }

    if (update_aZ == true) {
      mpu.set_acc_offset(Z_axis, aZ_offset);
    }

    //------ Check if each axis is adjusted -----
    const short maximum_error = 5;//set maximum deviation to 5 LSB
    if ((mpu.ax - maximum_error) <= 0) {

    }

    if ((abs(mpu.ax) - maximum_error) <= 0) {
      update_aX = false;
    }

    if ((abs(mpu.ay) - maximum_error) <= 0) {
      update_aY = false;
    }

    if ((abs(mpu.az) - maximum_error) <= 0) {
      update_aZ = false;
    }

    if ((abs(mpu.gx) - maximum_error) <= 0) {
      update_gX = false;
    }

    if ((abs(mpu.gy) - maximum_error) <= 0) {
      update_gY = false;
    }

    if ((abs(mpu.gz) - maximum_error) <= 0) {
      update_gZ = false;
    }


    //------ Adjust procedure end condition ------
    if (update_gX == false && update_gY == false && update_gZ == false && update_aX == false && update_aY == false && update_aZ == false) {
      break;
    }

    delay(10);
  }

  //print the output values :
  //  Serial.println("Offset cancellation complete!");
  //  Serial.println();
  //  Serial.print("Accelerometer offset:  X: ");
  //  dataString += aX_offset;
  //  dataString += aY_offset;
  //  dataString += aZ_offset;
  //  dataString += gX_offset;
  //  dataString += gY_offset;
  //  dataString += gZ_offset;

  //print data
  dataString += mpu.ax;
  dataString += aX_offset;
  dataString += mpu.ay;
  dataString += aY_offset;
  dataString += mpu.az;
  dataString += aZ_offset;
  dataString += mpu.gx;
  dataString += gX_offset;
  dataString += gY_offset;
  dataString += mpu.gy;
  dataString += mpu.gz;
  dataString += gZ_offset;
}

void setup()
{
  Serial.begin(115200);
  mySerial.begin(9600);

  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }


  // --------------------------------- START OF MPU-9255 ---------------------------------
  if (mpu.init()) {
    //    Serial.println("initialization failed");
  } else {
    //    Serial.println("initialization succesful!");
  }
  //print some control readings
  //  print_data();
  //adjust offset
  //  Serial.println("Adjusting offset...");
  adjust_offset();
  delay(10000);
  // --------------------------------- END OF MPU-9255 ---------------------------------


  // --------------------------------- END OF DHT11 ---------------------------------
  dht.begin();
  sensor_t sensor;
  dht.temperature().getSensor(&sensor);
  //  Serial.println(F("------------------------------------"));
  //  Serial.println(F("Temperature Sensor"));
  //  Serial.print  (F("Sensor Type: ")); Serial.println(sensor.name);
  //  Serial.print  (F("Driver Ver:  ")); Serial.println(sensor.version);
  //  Serial.print  (F("Unique ID:   ")); Serial.println(sensor.sensor_id);
  //  Serial.print  (F("Max Value:   ")); Serial.print(sensor.max_value); Serial.println(F("°C"));
  //  Serial.print  (F("Min Value:   ")); Serial.print(sensor.min_value); Serial.println(F("°C"));
  //  Serial.print  (F("Resolution:  ")); Serial.print(sensor.resolution); Serial.println(F("°C"));
  //  Serial.println(F("------------------------------------"));
  // Print humidity sensor details.
  dht.humidity().getSensor(&sensor);
  //  Serial.println(F("Humidity Sensor"));
  //  Serial.print  (F("Sensor Type: ")); Serial.println(sensor.name);
  //  Serial.print  (F("Driver Ver:  ")); Serial.println(sensor.version);
  //  Serial.print  (F("Unique ID:   ")); Serial.println(sensor.sensor_id);
  //  Serial.print  (F("Max Value:   ")); Serial.print(sensor.max_value); Serial.println(F("%"));
  //  Serial.print  (F("Min Value:   ")); Serial.print(sensor.min_value); Serial.println(F("%"));
  //  Serial.print  (F("Resolution:  ")); Serial.print(sensor.resolution); Serial.println(F("%"));
  //  Serial.println(F("------------------------------------"));
  // Set delay between sensor readings based on sensor details.
  delayMS = sensor.min_delay / 1000;
  // --------------------------------- END OF DHT11 ---------------------------------


  Wire.begin();
  Wire.begin(11);
  Wire.onReceive(receiveEvent);


  // --------------------------------- START OF BMX280 ---------------------------------
  //begin() checks the Interface, reads the sensor ID (to differentiate between BMP280 and BME280)
  //and reads compensation parameters.
  if (!bmx280.begin()) {
    //    Serial.println("begin() failed. check your BMx280 Interface and I2C Address.");
    //    while (1);
  }

  //reset sensor to default parameters.
  bmx280.resetToDefaults();

  //by default sensing is disabled and must be enabled by setting a non-zero
  //oversampling setting.
  //set an oversampling setting for pressure and temperature measurements.
  bmx280.writeOversamplingPressure(BMx280MI::OSRS_P_x16);
  bmx280.writeOversamplingTemperature(BMx280MI::OSRS_T_x16);

  //if sensor is a BME280, set an oversampling setting for humidity measurements.
  if (bmx280.isBME280()) bmx280.writeOversamplingHumidity(BMx280MI::OSRS_H_x16);
  // --------------------------------- END OF BMX280 ---------------------------------


  // --------------------------------- START OF SD Module ---------------------------------
  //  Serial.print("Initializing SD card...");
  // see if the card is present and can be initialized:
  if (!SD.begin(chipSelect)) {
    //    Serial.println("Card failed, or not present");
    // don't do anything more:
    while (1);
  }
  //  Serial.println("card initialized.");
  // --------------------------------- END OF SD Module ---------------------------------


  Serial.println(F("\nSeed17 Project"));
  Serial.println(F("Questions: info@seed17.eu\n"));
}

void loop() {
  // info writed to SD, and communication between Master - Slave
  dataString = "";


  // GPS <----------------------- START
  bool ready = false;
  if (mySerial.available()) {
    //    Serial.println("Inside of if");
    char c = mySerial.read();
    if (gps.encode(c)) {
      ready = true;
    }
  }
  long lat, lon;
  float flat, flon;
  unsigned long age;
  if (ready) {
    gps.get_position(&lat, &lon, &age);
    gpsPosition = long(gps.altitude());
    gps.f_get_position(&flat, &flon, &age);
    printFloat(flat, 5);
    //    Serial.print(", ");
    printFloat(flon, 5);
    //    Serial.print(", ");
    Serial.print(gpsPosition / 100);
  }
  if (location.length() > 100) {
    location = "";
  }
  // GPS <----------------------- END


  // DHT11 <----------------------- START
  delay(delayMS);
  // Get temperature event and print its value.
  sensors_event_t event;
  dht.temperature().getEvent(&event);
  if (isnan(event.temperature)) {
    //    Serial.println(F("Error reading temperature!"));
  } else {
    //    Serial.print(F("Temperature: "));
    //    Serial.print(event.temperature);
    dataString += "TemperatureDHT: " + String(event.temperature) + ", ";
    //    Serial.println(F("°C"));
  }
  // Get humidity event and print its value.
  dht.humidity().getEvent(&event);
  if (isnan(event.relative_humidity)) {
    //    Serial.println(F("Error reading humidity!"));
  } else {
    //    Serial.print(F("Humidity: "));
    //    Serial.print(event.relative_humidity);
    dataString += "Humidity: " + String(event.relative_humidity) + ", ";
    //    Serial.println(F("%"));
  }
  // DHT11 <----------------------- END


  // MPU-9255 <-------------------- START
  //  print_data();
  //dataString += print_data();
  // MPU-9255 <-------------------- END


  // BMX280 <---------------------- START
  if (!bmx280.measure()) {
    //    Serial.println("could not start measurement, is a measurement already running?");
    return;
  }
  //wait for the measurement to finish
  do {
    delay(100);
  } while (!bmx280.hasValue());
  //  Serial.print("Pressure: "); Serial.println(bmx280.getPressure());
  dataString += " Pressure: " + String(bmx280.getPressure()) + ", ";
  //  Serial.print("Temperature: "); Serial.println(bmx280.getTemperature());
  dataString += " TemperatureBMX280: " + String(bmx280.getTemperature()) + ", ";
  // BMX280 <---------------------- END

  // --------------------------------- START OF SD Module ---------------------------------
  // open the file. note that only one file can be open at a time,
  // so you have to close this one before opening another.
  File dataFile = SD.open("datalog.txt", FILE_WRITE);
  // if the file is available, write to it:
  if (dataFile) {
    dataFile.println(dataString);
    dataFile.close();
    // print to the serial port too:
    //    Serial.println(dataString);
  }
  // if the file isn't open, pop up an error:
  else {
    //    Serial.println("error opening datalog.txt");
  }
  // --------------------------------- END OF SD Module ---------------------------------


  if (Send == true) {
    Wire.beginTransmission(8); // transmit to device #8
    Wire.write(5);
    //    Serial.println("sending 5");
    Wire.endTransmission();    // stop transmitting
    Send = false;
  }
}

void receiveEvent(int howMany) {
  if (Wire.available()) {
    if (Wire.read() == 'A') {
      //      Serial.println("\nInput signal...Yes, i am Slave A");
      Send = true;
    }
  }
}

void printFloat(double number, int digits) {
  // Handle negative numbers
  if (number < 0.0) {
    //    Serial.print('-');
    number = -number;
  }

  // Round correctly so that print(1.999, 2) prints as "2.00"
  double rounding = 0.5;
  for (uint8_t i = 0; i < digits; ++i) rounding /= 10.0;

  number += rounding;

  // Extract the integer part of the number and print it
  unsigned long int_part = (unsigned long)number;
  double remainder = number - (double)int_part;
  //  Serial.print(int_part);

  // Print the decimal point, but only if there are digits beyond
  if (digits > 0)
    //    Serial.print(".");
    // Extract digits from the remainder one at a time
    while (digits-- > 0) {
      remainder *= 10.0;
      int toPrint = int(remainder);
      //    Serial.print(toPrint);
      remainder -= toPrint;
    }
}
