#include <Wire.h>
#include <BMx280MI.h> //gy-91
#include <MPU9255.h>  //gy-91
#include <Adafruit_Sensor.h>
#include <DHT.h>
#include <DHT_U.h>

#define I2C_ADDRESS 0x76
#define DHTPIN A0
#define DHTTYPE    DHT11

//create a objects
BMx280I2C bmx280(I2C_ADDRESS);
MPU9255 mpu;
DHT_Unified dht(DHTPIN, DHTTYPE);

int Send = false;
uint32_t delayMS;

void print_data() {
  mpu.read_acc();
  mpu.read_gyro();

  Serial.print("AX: ");
  Serial.print(mpu.ax);
  Serial.print(" AY: ");
  Serial.print(mpu.ay);
  Serial.print(" AZ: ");
  Serial.print(mpu.az);
  Serial.print("    GX: ");
  Serial.print(mpu.gx);
  Serial.print(" GY: ");
  Serial.print(mpu.gy);
  Serial.print(" GZ: ");
  Serial.println(mpu.gz);
  delay(100);
}

void adjust_offset() {
  //set bandwidths to 5Hz to reduce the noise
  mpu.set_acc_bandwidth(acc_5Hz);
  mpu.set_gyro_bandwidth(gyro_5Hz);

  int16_t gX_offset = 0;//gyroscope X axis offset
  int16_t gY_offset = 0;//gyroscope Y axis offset
  int16_t gZ_offset = 0;//gyroscope Z axis offset

  int16_t aX_offset = 0;//accelerometer X axis offset
  int16_t aY_offset = 0;//accelerometer Y axis offset
  int16_t aZ_offset = 0;//accelerometer Z axis offset

  //update flags
  //gyroscope
  bool update_gX = true;
  bool update_gY = true;
  bool update_gZ = true;

  //accelerometer
  bool update_aX = true;
  bool update_aY = true;
  bool update_aZ = true;

  //discard the first reading
  mpu.read_acc();
  mpu.read_gyro();
  delay(10);

  while (1) //offset adjusting loop
  {
    mpu.read_acc();
    mpu.read_gyro();

    //-------- adjust accelerometer X axis offset ----------

    if (mpu.ax > 0 && update_aX == true) {
      aX_offset --;//decrement offset
    }


    if (mpu.ax < 0 && update_aX == true)  {
      aX_offset ++;//increment offset
    }

    //-------- adjust accelerometer Y axis offset ----------

    if (mpu.ay > 0 && update_aY == true) {
      aY_offset --;//decrement offset
    }

    if (mpu.ay < 0 && update_aY == true) {
      aY_offset ++;//increment offset
    }

    //-------- adjust accelerometer Z axis offset ----------

    if (mpu.az > 0 && update_aZ == true) {
      aZ_offset --;//decrement offset
    }

    if (mpu.az < 0 && update_aZ == true)
    {
      aZ_offset ++;//increment offset
    }

    //-------- adjust gyroscope X axis offset ----------

    if (mpu.gx > 0 && update_gX == true) {
      gX_offset --;//decrement offset
    }

    if (mpu.gx < 0 && update_gX == true) {
      gX_offset ++;//increment offset
    }

    //-------- adjust gyroscope Y axis offset ----------

    if (mpu.gy > 0 && update_gY == true) {
      gY_offset --;//decrement offset
    }

    if (mpu.gy < 0 && update_gY == true) {
      gY_offset ++;//increment offset
    }

    //-------- adjust gyroscope Z axis offset ----------

    if (mpu.gz > 0 && update_gZ == true) {
      gZ_offset --;//decrement offset
    }

    if (mpu.gz < 0 && update_gZ == true) {
      gZ_offset ++;//increment offset
    }

    //print data
    Serial.print("AX: ");
    Serial.print(mpu.ax);
    Serial.print(" (Offset: ");
    Serial.print(aX_offset);
    Serial.print(" ) ");
    Serial.print(" AY: ");
    Serial.print(mpu.ay);
    Serial.print(" (Offset: ");
    Serial.print(aY_offset);
    Serial.print(" ) ");
    Serial.print(" AZ: ");
    Serial.print(mpu.az);
    Serial.print(" (Offset: ");
    Serial.print(aZ_offset);
    Serial.print(" ) ");
    Serial.print("    GX: ");
    Serial.print(mpu.gx);
    Serial.print(" (Offset: ");
    Serial.print(gX_offset);
    Serial.print(" ) ");
    Serial.print(" GY: ");
    Serial.print(" (Offset: ");
    Serial.print(gY_offset);
    Serial.print(" ) ");
    Serial.print(mpu.gy);
    Serial.print(" GZ: ");
    Serial.print(mpu.gz);
    Serial.print(" (Offset: ");
    Serial.print(gZ_offset);
    Serial.println(" ) ");

    //set new offset
    if (update_gX == true) {
      mpu.set_gyro_offset(X_axis, gX_offset);
    }

    if (update_gY == true) {
      mpu.set_gyro_offset(Y_axis, gY_offset);
    }

    if (update_gZ == true) {
      mpu.set_gyro_offset(Z_axis, gZ_offset);
    }

    if (update_aX == true) {
      mpu.set_acc_offset(X_axis, aX_offset);
    }

    if (update_aY == true) {
      mpu.set_acc_offset(Y_axis, aY_offset);
    }

    if (update_aZ == true) {
      mpu.set_acc_offset(Z_axis, aZ_offset);
    }

    //------ Check if each axis is adjusted -----
    const short maximum_error = 5;//set maximum deviation to 5 LSB
    if ((mpu.ax - maximum_error) <= 0) {

    }

    if ((abs(mpu.ax) - maximum_error) <= 0) {
      update_aX = false;
    }

    if ((abs(mpu.ay) - maximum_error) <= 0) {
      update_aY = false;
    }

    if ((abs(mpu.az) - maximum_error) <= 0) {
      update_aZ = false;
    }

    if ((abs(mpu.gx) - maximum_error) <= 0) {
      update_gX = false;
    }

    if ((abs(mpu.gy) - maximum_error) <= 0) {
      update_gY = false;
    }

    if ((abs(mpu.gz) - maximum_error) <= 0) {
      update_gZ = false;
    }


    //------ Adjust procedure end condition ------
    if (update_gX == false && update_gY == false && update_gZ == false && update_aX == false && update_aY == false && update_aZ == false) {
      break;
    }

    delay(10);
  }

  //print the output values :
  Serial.println("Offset cancellation complete!");
  Serial.println();
  Serial.print("Accelerometer offset:  X: ");
  Serial.print(aX_offset);
  Serial.print("  Y: ");
  Serial.print(aY_offset);
  Serial.print("  Z: ");
  Serial.println(aZ_offset);
  Serial.print("Gyroscope offset:   X: ");
  Serial.print(gX_offset);
  Serial.print("  Y: ");
  Serial.print(gY_offset);
  Serial.print("  Z: ");
  Serial.println(gZ_offset);
  Serial.println();
}

void setup()
{
  Serial.begin(115200);

  // --------------------------------- START OF MPU-9255 ---------------------------------
  if (mpu.init()) {
    Serial.println("initialization failed");
  } else {
    Serial.println("initialization succesful!");
  }
  //print some control readings
  print_data();
  //adjust offset
  Serial.println("Adjusting offset...");
  adjust_offset();
  delay(10000);
  // --------------------------------- END OF MPU-9255 ---------------------------------


  // --------------------------------- END OF DHT11 ---------------------------------
  dht.begin();
  sensor_t sensor;
  dht.temperature().getSensor(&sensor);
  Serial.println(F("------------------------------------"));
  Serial.println(F("Temperature Sensor"));
  Serial.print  (F("Sensor Type: ")); Serial.println(sensor.name);
  Serial.print  (F("Driver Ver:  ")); Serial.println(sensor.version);
  Serial.print  (F("Unique ID:   ")); Serial.println(sensor.sensor_id);
  Serial.print  (F("Max Value:   ")); Serial.print(sensor.max_value); Serial.println(F("°C"));
  Serial.print  (F("Min Value:   ")); Serial.print(sensor.min_value); Serial.println(F("°C"));
  Serial.print  (F("Resolution:  ")); Serial.print(sensor.resolution); Serial.println(F("°C"));
  Serial.println(F("------------------------------------"));
  // Print humidity sensor details.
  dht.humidity().getSensor(&sensor);
  Serial.println(F("Humidity Sensor"));
  Serial.print  (F("Sensor Type: ")); Serial.println(sensor.name);
  Serial.print  (F("Driver Ver:  ")); Serial.println(sensor.version);
  Serial.print  (F("Unique ID:   ")); Serial.println(sensor.sensor_id);
  Serial.print  (F("Max Value:   ")); Serial.print(sensor.max_value); Serial.println(F("%"));
  Serial.print  (F("Min Value:   ")); Serial.print(sensor.min_value); Serial.println(F("%"));
  Serial.print  (F("Resolution:  ")); Serial.print(sensor.resolution); Serial.println(F("%"));
  Serial.println(F("------------------------------------"));
  // Set delay between sensor readings based on sensor details.
  delayMS = sensor.min_delay / 1000;
  // --------------------------------- END OF DHT11 ---------------------------------


  Wire.begin();
  Wire.begin(11);
  Wire.onReceive(receiveEvent);


  // --------------------------------- START OF BMX280 ---------------------------------
  //begin() checks the Interface, reads the sensor ID (to differentiate between BMP280 and BME280)
  //and reads compensation parameters.
  if (!bmx280.begin()) {
    Serial.println("begin() failed. check your BMx280 Interface and I2C Address.");
    //    while (1);
  }

  //reset sensor to default parameters.
  bmx280.resetToDefaults();

  //by default sensing is disabled and must be enabled by setting a non-zero
  //oversampling setting.
  //set an oversampling setting for pressure and temperature measurements.
  bmx280.writeOversamplingPressure(BMx280MI::OSRS_P_x16);
  bmx280.writeOversamplingTemperature(BMx280MI::OSRS_T_x16);

  //if sensor is a BME280, set an oversampling setting for humidity measurements.
  if (bmx280.isBME280()) bmx280.writeOversamplingHumidity(BMx280MI::OSRS_H_x16);
  // --------------------------------- END OF BMX280 ---------------------------------


  Serial.println(F("\nSeed17 Project"));
  Serial.println(F("Questions: info@seed17.eu\n"));
}

void loop() {
  // DHT11 <----------------------- START
  delay(delayMS);
  // Get temperature event and print its value.
  sensors_event_t event;
  dht.temperature().getEvent(&event);
  if (isnan(event.temperature)) {
    Serial.println(F("Error reading temperature!"));
  } else {
    Serial.print(F("Temperature: "));
    Serial.print(event.temperature);
    Serial.println(F("°C"));
  }
  // Get humidity event and print its value.
  dht.humidity().getEvent(&event);
  if (isnan(event.relative_humidity)) {
    Serial.println(F("Error reading humidity!"));
  } else {
    Serial.print(F("Humidity: "));
    Serial.print(event.relative_humidity);
    Serial.println(F("%"));
  }
  // DHT11 <----------------------- END


  // MPU-9255 <-------------------- START 
  print_data(); 
  // MPU-9255 <-------------------- END


  // BMX280 <---------------------- START
  if (!bmx280.measure()) {
    Serial.println("could not start measurement, is a measurement already running?");
    return;
  }
  //wait for the measurement to finish
  do {
    delay(100);
  } while (!bmx280.hasValue());
  Serial.print("Pressure: "); Serial.println(bmx280.getPressure());
  Serial.print("Temperature: "); Serial.println(bmx280.getTemperature()); 
  // BMX280 <---------------------- END


  if (Send == true) {
    Wire.beginTransmission(8); // transmit to device #8
    Wire.write(5);
    Serial.println("sending 5");
    Wire.endTransmission();    // stop transmitting
    Send = false;
  }
}

void receiveEvent(int howMany) {
  if (Wire.available()) {
    if (Wire.read() == 'A') {
      Serial.println("\nInput signal...Yes, i am Slave A");
      Send = true;
    }
  }
}
