
#include <SPI.h>
#include <SD.h>
#include <SoftwareSerial.h>
#include <TinyGPS.h>

SoftwareSerial mySerial(7, 8); // RX, TX
TinyGPS gps;
File dataFile;

const int chipSelect = 4;
String dataString = "", location = "";
long int gpsPosition = 0;
bool flag = true;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  mySerial.begin(9600);

  Serial.println("\nInside of setup()\n");

  if (!SD.begin(chipSelect)) {
    Serial.println("Card failed, or not present");
    // don't do anything more:
    while (1);
  }
}

void loop() {
  // put your main code here, to run repeatedly:
  if (flag) Serial.println("\n Inside of loop()\n");
  flag = false;

  // GPS <----------------------- START
  bool ready = false;
  if (mySerial.available()) {
    //    Serial.println("Inside of if");
    char c = mySerial.read();
    if (gps.encode(c)) {
      ready = true;
    }
  }
  long lat, lon;
  float flat, flon;
  unsigned long age;
  if (ready) {
    gps.get_position(&lat, &lon, &age);
    gpsPosition = long(gps.altitude());
    gps.f_get_position(&flat, &flon, &age);
    dataString += " lat" + String(printFloat(flat, 5));
    //    Serial.print(", ");
    dataString += " lon" + String(printFloat(flon, 5));
    //    Serial.print(", ");
    //    Serial.print(gpsPosition / 100);
    dataString += " hi" + String((gpsPosition / 100));
    Serial.println("GPS data extracted from module: " + dataString);
  }
  if (location.length() > 100) {
    location = "";
  }

  // GPS <----------------------- END

  if (dataString != "") {
    dataFile = SD.open("datalog.txt", FILE_WRITE);
    // if the file is available, write to it:
    if (dataFile) {
      dataFile.println(dataString);
      dataFile.close();
      // print to the serial port too:
      Serial.println("Writed to SD: " + dataString);
    } else {
      Serial.println("error opening datalog.txt");
    }
  }
  dataString = "";
}

String printFloat(double number, int digits) {
  String dataGPS = "";
  // Handle negative numbers
  if (number < 0.0) {
    //    Serial.print('-');
    number = -number;
  }

  // Round correctly so that print(1.999, 2) prints as "2.00"
  double rounding = 0.5;
  for (uint8_t i = 0; i < digits; ++i) rounding /= 10.0;

  number += rounding;

  // Extract the integer part of the number and print it
  unsigned long int_part = (unsigned long)number;
  double remainder = number - (double)int_part;
  //  Serial.print(int_part);
  dataGPS += String(int_part) + ".";
  // Print the decimal point, but only if there are digits beyond
  if (digits > 0)
    //    Serial.print(".");
    // Extract digits from the remainder one at a time
    while (digits-- > 0) {
      remainder *= 10.0;
      int toPrint = int(remainder);
      //    Serial.print(toPrint);
      dataGPS += String(toPrint);
      remainder -= toPrint;
    }

  return dataGPS;
}
