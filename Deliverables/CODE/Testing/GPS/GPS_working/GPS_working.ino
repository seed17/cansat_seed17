#include <SoftwareSerial.h>
#include <TinyGPS.h>


///*********************
//  4 to GPS Module TX
//  3 to GPS Module RX
// *********************/
SoftwareSerial mySerial(8, 7); // RX, TX
TinyGPS gps;

String location = "";
long int gpsPosition = 0;

void setup()  {
  Serial.begin(115200);
  mySerial.begin(9600);
  Serial.println("Inside of setup()");
}

void loop() {
  Serial.println("Inside of loop()");
  bool ready = false;
  if (mySerial.available()) {
    Serial.println("Inside of if");
    char c = mySerial.read();

    if (gps.encode(c)) {
      ready = true;
    }
  }

  long lat, lon;
  float flat, flon;
  unsigned long age;

  if (ready) {
    gps.get_position(&lat, &lon, &age);
    gpsPosition = long(gps.altitude());

    gps.f_get_position(&flat, &flon, &age);
    printFloat(flat, 5);
    Serial.print(", ");
    printFloat(flon, 5);
    Serial.print(", ");
    Serial.print(gpsPosition / 100);

  }
  if (location.length() > 100) {
    location = "";
  }
}

void printFloat(double number, int digits)
{
  // Handle negative numbers
  if (number < 0.0)
  {
    Serial.print('-');
    number = -number;
  }

  // Round correctly so that print(1.999, 2) prints as "2.00"
  double rounding = 0.5;
  for (uint8_t i = 0; i < digits; ++i)
    rounding /= 10.0;

  number += rounding;

  // Extract the integer part of the number and print it
  unsigned long int_part = (unsigned long)number;
  double remainder = number - (double)int_part;
  Serial.print(int_part);

  // Print the decimal point, but only if there are digits beyond
  if (digits > 0)
    Serial.print(".");

  // Extract digits from the remainder one at a time
  while (digits-- > 0)
  {
    remainder *= 10.0;
    int toPrint = int(remainder);
    Serial.print(toPrint);
    remainder -= toPrint;
  }
}
