/*Download Lora.h from: https://github.com/sandeepmistry/arduino-LoRa 
 * Pin connections according to: https://www.likecircuit.com/433mhz-lora-ra-02-arduino-switch-led/ 
 * Lora is connected to: MASTER Atmega
If Lora connection is right, it will start sending packages.
Pin connections:(D stands for Digital Pin of Atmega)

Atmega pins => Lora Pins
D13 => SCK
D12 => MISO
D11 => MOSI
D10 => NSS
D9  => RESET
D2  => DIO0

Lora works with 3.3V, except if its module uses input of 5V
At most Lora modules, its Gnd pins are shorted, so you only need to connect one pin to GND

*/

#include <SPI.h>
#include <LoRa.h>
 
void setup() {
  Serial.begin(9600);
  while (!Serial);
 
  Serial.println("LoRa Sender");
 
  if (!LoRa.begin(433E6)) {
    Serial.println("Starting LoRa failed!");
    while (1);
  }
 
  LoRa.setSpreadingFactor(10);
  LoRa.setSignalBandwidth(62.5E3);
  LoRa.crc();

}
 
void loop() {
    Serial.println("Send packet");
    // send packet
    LoRa.beginPacket();
    LoRa.print("1");
    LoRa.endPacket();
  delay(500);
}
